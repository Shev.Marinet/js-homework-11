let id = null;
let seconds = null;
function validate(id) {
    while (true){
        if (isNaN(id)){
            id = prompt(`${id}  isn't a number. Enter number of items in the list.`)
        }
        else {
            break;
        }
    }
    return id;
}
function startTimer(duration, display) {
    let timer = duration;
    setInterval(function () {
        seconds = timer--;
        seconds = seconds < 10 ? "0" + seconds : seconds;
        display.textContent = seconds;
    }, 1000);
}

function addList() {
    const arr = [];
    id = prompt(`Enter number of items in the list.`);
    id = validate(id);
    for ( let i = 0; i < id; i++ ){
        arr[i] = prompt(`Enter ${i+1} item of the list from ${id}`);
    }
    const ol = document.createElement('ol');
    ol.id ='list';

    document.body.insertBefore(ol, document.body.firstChild);
    let values = [].map.call(arr, function(text) {
        const newLi = document.createElement('li');
        document.querySelector('#list').appendChild(newLi);
        newLi.innerText = text;
    });
    const div =document.createElement('div');
    document.body.insertBefore(div, document.body.lastChild);
    div.innerHTML = `The list will disappear in <span id="time">10</span> seconds!`;
    const   display = document.querySelector('#time');
    startTimer(10, display);
    setTimeout(function() {
        ol.parentElement.removeChild(ol);
        div.parentElement.removeChild(div);
    }, 11000);
}

addList();